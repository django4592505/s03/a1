from django.db import models

# Create your models here.

class ToDoItem(models.Model):
	# CharField = String value
	# DateTimeField =  DateTime datatype; need to set dateTime as it's not automatic
	# check Django documentation for other command
	task_name = models.CharField(max_length = 50)
	description = models.CharField(max_length = 200)
	status = models.CharField(max_length = 50, default = "Pending")
	date_created = models.DateTimeField("Date Created")